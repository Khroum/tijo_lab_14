package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class IsCorrectSizeSpec extends  Specification{

    @Unroll
    def "should return #expectedAnswer for PESEL: #id"(){

        given: "user id"
            def userId = new UserId(id)


        when: "check if size equals 11"
            def result = userId.isCorrectSize()

        then: "result should equal expected answer"
            result == expectedAnswer

        where:
            id | expectedAnswer
            "00000000000" | true
            "12312312312" | true
            "000 000 000 00" | false
            "123123123" | false
            "123 567 9  " | true
            "" | false
            null | false
    }
}
