package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class IsCorrectSpec extends Specification {

    @Unroll
    def "should return true for id: #id"(){
        given:"correct id"
            def userId = new UserId(id)

        when:"when correctness is checked"
            def result = userId.isCorrect()

        then:"result should be true"
            result

        where:"using random correct pesel numbers"
            id << ["70081652689",
                   "83051598298",
                   "71071571522",
                   "89061713135",
                   "47071499821",
                   "04281779178"]
    }

    @Unroll
    def "should return false for id: #id"(){
        given:"incorrect id"
        def userId = new UserId(id)

        when:"when correctness is checked"
        def result = userId.isCorrect()

        then:"result should be false"
            !result

        where:"using random incorrect pesel numbers"
        id << ["700816a2689",
               "8305159a298",
               "71071571a22",
               "8906171313a",
               "93051036726",
               "91112832214",
               "76021656553",
               "           ",
               "",
               null]
    }
}
