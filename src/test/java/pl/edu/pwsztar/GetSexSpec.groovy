package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

import static pl.edu.pwsztar.UserIdChecker.Sex.MAN
import static pl.edu.pwsztar.UserIdChecker.Sex.WOMAN


class GetSexSpec extends Specification {

    @Unroll
    def "should return sex: #expectedAnswer, isPresent: true for id: #id "(){
        given:"user id"
            def userId = new UserId(id)

        when:"sex is get"
            def result = userId.getSex()
        then:"the result should be present and equal to expected answer "
            result.isPresent() && result.get() == expectedAnswer
        where:
            id | expectedAnswer
            "82121435927" | WOMAN
            "71012988318" | MAN
    }

    @Unroll
    def "should get empty optional for id: #id"(){
        given:"id with incorrect sign, empty string or null"
            def userId = new UserId(id)

        when:"sex is get"
            def result = userId.getSex().get()

        then:"it should throw exception when .get() is used"
            thrown NoSuchElementException

        where:
            id << ["123123123a1","",null]
    }
}
