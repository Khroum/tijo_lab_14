package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll



class GetDateSpec extends Specification {

    @Unroll
    def "should return date: #expectedAnswer, for id: #id "(){
        given:"user id"
            def userId = new UserId(id)

        when:"date is get"
            def result = userId.getDate()

        then:"the result should be present and equal to expected answer "
            result.isPresent() && result.get() == expectedAnswer

        where:
            id | expectedAnswer
            "90220323177" | "03-02-2090"
            "04041474646" | "14-04-1904"
    }

    @Unroll
    def "should get empty optional for #id"(){
        given:"user id with incorrect date or signs"
            def userId = new UserId(id)

        when:"date is get"
            def result = userId.getDate().get()

        then:"it should throw exception when .get() is used"
            thrown NoSuchElementException

        where:
            id << ["90014055555","90411055555","aa101055555","10aa1055555","1010aa55555"]
    }
}
