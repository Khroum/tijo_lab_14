package pl.edu.pwsztar;




import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;



final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL
    private static final int [] wages = {9,7,3,1,9,7,3,1,9,7};

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
       if(isNullOrEmpty()){
           return false;
       }

       return id.length()==11;
    }

    @Override
    public Optional<Sex> getSex() {
        if(isCorrect()){
            int number = Integer.parseInt(id.charAt(9)+"");
            return  Optional.of(number%2==0? Sex.WOMAN : Sex.MAN);
        }

        return Optional.empty();
    }

    @Override
    public boolean isCorrect() {
        if (!isCorrectSize() || hasIncorrectSigns()) {
            return false;
        }

        String dateToCheck = idToDate();
        if(!isDateCorrect(dateToCheck)){
            return false;
        }

        int controlSign = Integer.parseInt(id.charAt(10)+"");
        int controlSum = 0;

        for(int i=0;i<wages.length;i++){
            controlSum+=Integer.parseInt(id.charAt(i)+"")*wages[i];
        }

        return controlSum%10==controlSign;
    }

    @Override
    public Optional<String> getDate() {
        if(isCorrect()){
            return Optional.of(idToDate());
        }

        return Optional.empty();
    }

    private boolean isNullOrEmpty() {
        return id == null || id.isEmpty();
    }

    private boolean hasIncorrectSigns() {
        return  id
                .chars()
                .filter(c->!Character.isDigit(c))
                .findFirst()
                .isPresent();
    }

    private boolean isDateCorrect(String dateStr)  {
        try {
            SimpleDateFormat sdfrmt = new SimpleDateFormat("dd-MM-yyyy");
            sdfrmt.setLenient(false);
            sdfrmt.parse(dateStr);
            return true;
        }catch (ParseException pe) {
            return false;
        }
    }

    private String idToDate(){
        String year = id.substring(0,2);
        String month = id.substring(2,4);
        String day = id.substring(4,6);


        int monthToCheck = Integer.parseInt(month);

        if (monthToCheck>80 && monthToCheck<93){
            month = (monthToCheck-80)+"";
            year = "18"+year;
        }
        else if(monthToCheck>60 && monthToCheck<73){
            month = (monthToCheck-60)+"";
            year = "22"+year;
        }
        else  if (monthToCheck>40 && monthToCheck<53){
            month = (monthToCheck-60)+"";
            year = "21"+year;
        }
        else if (monthToCheck>20 && monthToCheck<33){
            month = (monthToCheck-20)+"";
            year = "20"+year;
        }
        else if (monthToCheck>0 && monthToCheck<13){
            year = "19"+year;
        }

        if(month.length()==1){
            month = '0'+month;
        }
        return day+'-'+month+'-'+year;
    }
}
